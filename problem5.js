function problem5(inventory) {
    const carYears = inventory.map((item) => item.car_year)
    const yearsOlderThan2000 = carYears.filter((item) => item < 2000)
    return yearsOlderThan2000;
}
    module.exports = problem5;
  