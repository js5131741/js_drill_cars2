function problem4(inventory) {
    const carYears = inventory.map((item) => item.car_year)
    return carYears;
  }
  
  module.exports = problem4;
  