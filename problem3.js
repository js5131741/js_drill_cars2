function problem3(inventory) {
    const carModels = inventory.map((item) => item.car_model)
    carModels.sort();
    return carModels;
  }
  
  module.exports = problem3;
  