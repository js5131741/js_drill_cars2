const problem1 = require('../problem1');
const inventory = require('../Inventory'); // Import inventory data

const carId = 33;
const car = problem1(inventory, carId);

car ?  console.log(`Car ${carId} is a ${car.car_year} ${car.car_make} ${car.car_model}`) : console.log(`Car with ID ${carId} not found in the inventory.`);
