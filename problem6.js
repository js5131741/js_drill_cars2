function problem6(inventory) {
    const filteredCars = inventory.filter(
      (car) => car.car_make === 'BMW' || car.car_make === 'Audi'
    );
    return filteredCars;
  }
  
  module.exports = problem6;
  