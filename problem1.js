function problem1(inventory, id) {
    const car = inventory.filter((item) => item.id === id)[0];
    return car;
}

module.exports = problem1